const MenuItem = require('../models/menuItem');
const User = require('../models/user');

module.exports.getAllMenuItems = (req, res, next) => MenuItem.findAll().then(items => res.status(201).json(items));

module.exports.getCategories = (req, res, next) => MenuItem.aggregate('Category', 'DISTINCT', {plain: false}).then(items => res.status(201).json(items))

module.exports.getItemsPerCategory = (req, res, next) => {
    const category = req.params.category;
    MenuItem.findAll({where: {Category: category}}).then(items => res.status(201).json(items));
};

module.exports.uploadOrder = (req, res, next) => {
    const name = req.body.name;
    const address = req.body.address;
    const phone = req.body.phone;
    const uid = req.body.uid;
    const basket = req.body.basket;
    const basketItems = [];

    for (const basketElement of basket) {
        console.log(basketElement.id);
        MenuItem.findByPk(basketElement.id).then(item => {
            item.OrderItem = { quantity: basketElement.Qty };
            basketItems.push(item)})
            .then(res =>
            console.log('BASKET ITEMS: ', basketItems))
    }

    User.findByPk(uid)
        .then(user => user.createOrder({name: name, address: address, tel: phone}))
        .then(order =>
            order.addMenuItems(basketItems))
        .then(ready => res.status(201).json({message: 'Order saved into database'}))
        .catch(error => console.log(error))
};
