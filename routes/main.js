const express = require('express');

const feedController = require('../controllers/feed');

const router = express.Router();

router.get('/menuitems', feedController.getAllMenuItems);

router.get('/categories', feedController.getCategories);

router.get('/category/:category', feedController.getItemsPerCategory);

router.post('/sendorder', feedController.uploadOrder);


module.exports = router;
