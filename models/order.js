const Sequelize = require('sequelize');

const sequelize = require('../util/database');

const Order = sequelize.define('Order', {
    id: {
        type: Sequelize.DataTypes.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
    },
    name: {
        type: Sequelize.DataTypes.STRING(120),
        allowNull: false
    },
    address: {
        type: Sequelize.DataTypes.STRING,
        allowNull: false
    },
    tel: {
        type: Sequelize.DataTypes.STRING(120)
    }
});


module.exports = Order;
