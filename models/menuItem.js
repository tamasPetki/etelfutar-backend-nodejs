const Sequelize = require('sequelize');

const sequelize = require('../util/database');

const Order = require('./order');

const MenuItem = sequelize.define('MenuItem', {
    id: {
        type: Sequelize.DataTypes.INTEGER(11).UNSIGNED,
        allowNull: false,
        primaryKey: true
    },
    Category: {
        type: Sequelize.DataTypes.STRING(120),
        allowNull: false,
        defaultValue: ''
    },
    Description: {
        type: Sequelize.DataTypes.STRING(255),
        allowNull: true,
        defaultValue: ''
    },
    Name: {
        type: Sequelize.DataTypes.STRING(120),
        allowNull: false,
        defaultValue: ''
    },
    Price: {
        type: Sequelize.DataTypes.INTEGER(4),
        allowNull: false
    },
    Spicy: {
        type: Sequelize.DataTypes.INTEGER(1),
        allowNull: true
    },
    Vegatarian: {
        type: Sequelize.DataTypes.INTEGER(1),
        allowNull: true
    }
},{
    timestamps: false
});


module.exports = MenuItem;
